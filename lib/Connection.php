<?php
require_once "vendor/autoload.php";
Predis\Autoloader::register();

class Connection
{
  private $connection;
  private $db_origen;
  private $db_destino;
  protected $_dborigen;
  protected $_dbdest;
  protected $msgError;
  protected $redis;

  public function __construct()
  {
    $ini_file_origen = "config/database_origen.ini";
    $this->db_origen = (object)parse_ini_file($ini_file_origen);
    $ini_file_destino = "config/database_destino.ini";
    $this->db_destino = (object)parse_ini_file($ini_file_destino);
    $this->msgError = array();
    $this->redis = new Predis\Client();
  }

  public function connect_origen()
  {
    if ($this->_dborigen)
      return $this->_dborigen;
    
    $this->_dborigen = @mysql_connect($this->db_origen->dbhost, $this->db_origen->dbuser, $this->db_origen->dbpasswd);
    if (!$this->_dborigen)
      return false;
    else
      mysql_select_db($this->db_origen->dbname, $this->_dborigen);

    return $this->_dborigen;
  }

  public function connect_dest()
  {
    if ($this->_dbdest)
      return $this->_dbdest;
    
    $this->_dbdest = @mysql_connect($this->db_destino->dbhost, $this->db_destino->dbuser, $this->db_destino->dbpasswd, TRUE);
    if (!$this->_dbdest)
      return false;
    else
      mysql_select_db($this->db_destino->dbname, $this->_dbdest);

    return $this->_dbdest;
  }

  public function beginTransaction()
  {
    mysql_query("START TRANSACTION");
  }

  public function rollbackTransaction()
  {
    mysql_query("ROLLBACK");
  }


  public function commitTransaction()
  {
    mysql_query("COMMIT");
  }


  /*
  * Recupera el mensaje de error
  */
  public function getErrorMsg()
  {
    return $this->msgError;
  }

  /*
   * Ejecutar consulta select
   */
  public function executeSelect($query)
  {
    $response = new stdClass();
    $rows = array();
    if (empty($query)) {
      $response->success = false;
      $response->message = 'No hay select para ejecutar';
      return $response;
    }

    /* Configurar resultados a utf-8 */
    //mysql_query("SET CHARACTER SET utf8");
    mysql_query("SET NAMES 'utf8'");

    /* Ejecutar consulta */
    $result = @mysql_query($query, $this->_dborigen);
    if (!$result) {
      array_push($this->msgError, mysql_error());
      return false;
    }

    /* Recuperar registros */
    while ($row = @mysql_fetch_object($result)) {
      $rows[] = $row;
    }
    $response->success = true;
    $response->data = $rows;
    return $rows;
  }

  public function existeEmpresa($rfc)
  {
    $query = "select id from empresas where rfc = '" . $rfc . "'";

    $result = @mysql_query($query, $this->_dbdest);
    if (!$result) {
      return false;
    }
    else{
      $row = @mysql_fetch_object($result);
      return $row;
    }
  }

  public function close()
  {
    mysql_close($this->_dbdest);
    $this->_dbdest = false;
  }

  public function insert($table, $data, $idDir, $idEmpresa)
  {
    $id = null;
    switch($table) {
    case 'empresas':
      $sql = "insert into empresas (razon_social,rfc,nombre,curp,pagina_web,telefono_1,telefono_2,email,twitter,logotipo,xml2pdf_logo,nombre_contacto,telefono_contacto,email_contacto,posiciones_decimales_moneda,presicion_decimales,temas_id,regimen_tributarios_id,empresa_giros_id, direcciones_id, empresa_status_id,colorComprobante,codigo_moneda,paquetes_id,modo_impuestos,producto_predeterminado_id,importes_negativos,traslado_iva_predeterminado_id,servicios_adicionales,plantilla_mail,plantilla_mail_color,manejo_pedimentos,incluir_clave_producto,complementoOTD,complemento_leyenda_fiscal,fecha_registro,notas_confirmacion_recepcion,original_copia,incluir_domicilio_fiscal,control_pagos,cuentab_predeterminada,mantener_fecha_precomprobante,enajenacion_bienes,incluir_propina,agregarFirmas,email_config,sendCopyEmail)
          values ('".mysql_real_escape_string($data->razon_social)."','".$data->rfc."','".mysql_real_escape_string($data->nombre)."','".$data->curp."','".$data->pagina_web."','".$data->telefono_1."','".$data->telefono_2."','".$data->email."','".$data->twitter."','".$data->logotipo."','','".mysql_real_escape_string($data->nombre_contacto)."','".$data->telefono_contacto."','".$data->email_contacto."','".$data->posiciones_decimales_moneda."','".$data->presicion_decimales."','".$data->temas_id."','".$data->regimen_tributarios_id."','".$data->empresa_giros_id."','".$idDir."','".$data->empresa_status_id."','".$data->colorComprobante."','".$data->codigo_moneda."','".$data->paquetes_id."','".$data->modo_impuestos."','".$data->producto_predeterminado_id."','".$data->importes_negativos."','".$data->traslado_iva_predeterminado_id."','".$data->servicios_adicionales."','".$data->plantilla_mail."','".$data->plantilla_mail_color."','".$data->manejo_pedimentos."','".$data->incluir_clave_producto."',
          '".$data->complementoOTD."','".$data->complemento_leyenda_fiscal."','".$data->fecha_registro."','".$data->notas_confirmacion_recepcion."','".$data->original_copia."','".$data->incluir_domicilio_fiscal."','".$data->control_pagos."','".$data->cuentab_predeterminada."','".$data->mantener_fecha_precomprobante."','".$data->enajenacion_bienes."','".$data->incluir_propina."','".$data->agregarFirmas."','".$data->email_config."','".$data->sendCopyEmail."')";
      break;

    case 'sellos_digitales':
      $sql = "insert into sellos_digitales (nombre_certificado_csd,csd_sum,nombre_certificado_key,key_sum,password_csd,numero_serie,certificado,empresas_id,sucursales_id,fecha_alta,sellos_status_cancelacion,sellos_status_id,vigencia_inicial,vigencia_final,prioridad)
          values ('".$data->nombre_certificado_csd."','".$data->csd_sum."','".$data->nombre_certificado_key."','".$data->key_sum."','".$data->password_csd."','".$data->numero_serie."','".$data->certificado."','".$idEmpresa."',NULL,'".$data->fecha_alta."','".$data->sellos_status_cancelacion."','".$data->sellos_status_id."','".$data->vigencia_inicial."','".$data->vigencia_final."','".$data->prioridad."')";
      break;

    case 'folios':
      $sql = "insert into folios (serie,folio_inicial,folio_final,empresas_id,lugar_expedicion,folio_activo,folio_bloqueado,folio_status_id,sellos_digitales_id,tipodocumento_id)
          values ('".$data->serie."','".$data->folio_inicial."','".$data->folio_final."','".$idEmpresa."','".$data->lugar_expedicion."','".$data->folio_activo."','".$data->folio_bloqueado."','".$data->folio_status_id."','".$idDir."','".$data->tipodocumento_id."')";
      break;
  
    case 'direcciones':
      $estados = array(
        '0' => array('clave' => null),
        '1' => array('clave' => 'AGU'),
        '2' => array('clave' => 'BCN'),
        '3' => array('clave' => 'BCS'),
        '4' => array('clave' => 'CAM'),
        '5' => array('clave' => 'COA'),
        '6' => array('clave' => 'COL'),
        '7' => array('clave' => 'CHP'),
        '8' => array('clave' => 'CHH'),
        '9' => array('clave' => 'DIF'),
        '10' => array('clave' => 'DUR'),
        '11' => array('clave' => 'GUA'),
        '12' => array('clave' => 'GRO'),
        '13' => array('clave' => 'HID'),
        '14' => array('clave' => 'JAL'),
        '15' => array('clave' => 'MEX'),
        '16' => array('clave' => 'MIC'),
        '17' => array('clave' => 'MOR'),
        '18' => array('clave' => 'NAY'),
        '19' => array('clave' => 'NLE'),
        '20' => array('clave' => 'OAX'),
        '21' => array('clave' => 'PUE'),
        '22' => array('clave' => 'QUE'),
        '23' => array('clave' => 'ROO'),
        '24' => array('clave' => 'SLP'),
        '25' => array('clave' => 'SIN'),
        '26' => array('clave' => 'SON'),
        '27' => array('clave' => 'TAB'),
        '28' => array('clave' => 'TAM'),
        '20' => array('clave' => 'TLA'),
        '30' => array('clave' => 'VER'),
        '31' => array('clave' => 'YUC'),
        '32' => array('clave' => 'ZAC'),
        '33' => array('clave' => 'DIF')
      );

      $muni_id = $data->municipios_id;
      $cps = $this->searchByKeyPattern('cps', $data->codigo_postal.'::*');
      if (!empty($cps[1])) {
        $d = explode("::", array_keys($cps[1])[0]);
        if (!empty($d[2]))
          $muni_id = $d[2];
      }

      $sql = "insert into direcciones (calle,numero_interior,numero_exterior,colonia,localidad,estados_id,municipios_id,referencias,entre_calles,paises_id,codigo_postal)
          values ('".mysql_real_escape_string($data->calle)."','".$data->numero_interior."','".$data->numero_exterior."','".$data->colonia."','".$data->localidad."','".$estados[$data->estados_id]['clave']."','".$muni_id."','".mysql_real_escape_string($data->referencias)."','".mysql_real_escape_string($data->entre_calles)."','".$data->pais."','".$data->codigo_postal."')";
      break;

    case 'sucursales':
      $sql = "insert into sucursales (nombre,telefono,email,encargado,empresas_id,direcciones_id,colorComprobante,logotipo,xml2pdf_logo,temas_id,sucursales_status_id,plantilla_mail,plantilla_mail_color,codigo_sucursal)
          values ('".mysql_real_escape_string($data->nombre)."','".$data->telefono."','".$data->email."','".mysql_real_escape_string($data->encargado)."','".$idEmpresa."','".$idDir."','".$data->colorComprobante."','".$data->logotipo."','".$data->xml2pdf_logo."','".$data->temas_id."','".$data->sucursales_status_id."','".$data->plantilla_mail."','".$data->plantilla_mail_color."','".$data->codigo_sucursal."')";
      break;

    case 'empresa_regimenes_fiscales':
      $sql = "insert into empresa_regimenes_fiscales (empresas_id,regimen_fiscal_id)
        values ('".$idEmpresa."','".$data->regimen_fiscal_id."')";
      break;

    case 'empresas_complementos':
      $sql = "insert into empresas_complementos (empresas_id,complementos_id)
        values ('".$idEmpresa."','".$data->complementos_id."')";
      break;

    case 'cliente_impuestos':
      $sql = "insert into cliente_impuestos (clientes_id,impuestos_id,porcentaje)
        values ('".$idDir."','".$data->impuestos_id."','".$data->porcentaje."')";
      break;

    case 'clientes_alias':
      $sql = "insert into clientes_alias (clientes_id, alias,razon_social,direcciones_id,alias_status_id,email,email_bcc)
        values ('".$idEmpresa."','".mysql_real_escape_string($data->alias)."','".mysql_real_escape_string($data->razon_social)."','".$idDir."','".$data->alias_status_id."','".$data->email."','".$data->email_bcc."')";
      break;

    case 'clientes':
      $forma_id = 4;
      $sql = "insert into clientes (rfc,numero_id,razon_social,email,email_bcc,telefono_contacto,leyenda_terminos_condiciones,notas,notas_internas,direcciones_id,empresas_id,sucursales_id,codigo_moneda,codigo_lenguaje,regimen_tributarios_id,curp,cliente_status_id,temas_id,alias_general,forma_pagos_id,numero_cuenta_pago,validacion_email,validacion_email_bcc,emails_invalidos)
        values ('".$data->rfc."','".null."','".mysql_real_escape_string($data->razon_social)."','".$data->email."','".$data->email_bcc."','".$data->telefono_contacto."','".$data->leyenda_terminos_condiciones."','".mysql_real_escape_string($data->notas)."','".mysql_real_escape_string($data->notas_internas)."','".$idDir."','".$idEmpresa."',NULL,'".$data->codigo_moneda."','".$data->codigo_lenguaje."','".$data->regimen_tributarios_id."','".$data->curp."','".$data->cliente_status_id."','".$data->temas_id."','".$data->alias_general."','".$forma_id."','".$data->numero_cuenta_pago."','".$data->validacion_email."','".$data->validacion_email_bcc."','".$data->emails_invalidos."')";
      break;

    case 'productos':
      $sql = "insert into productos (clave_producto,descripcion,unidad_medida,precio_venta,codigo_barras,tipo,codigo_unificado,marca,modelo,imagen,notas,descuento,existencias,empresas_id,peso,productos_status_id,precio_emp,dsglc,tipo_version,numeroMotor,numeroSerie,color,anio,fraccion_arancelaria,submodelo,serie_mercancia,unidad_m_sat,clave_sat)
        values ('".$data->clave_producto."','".mysql_real_escape_string($data->descripcion)."','".$data->unidad_medida."','".$data->precio_venta."','".$data->codigo_barras."','".$data->tipo."','".$data->codigo_unificado."','".$data->marca."','".mysql_real_escape_string($data->modelo)."','".$data->imagen."','".mysql_real_escape_string($data->notas)."','".$data->descuento."','".$data->existencias."','".$idEmpresa."','".$data->peso."','".$data->productos_status_id."','".$data->precio_emp."','".$data->dsglc."','".$data->tipo_version."','".$data->numeroMotor."','".$data->numeroSerie."','".$data->color."','".$data->anio."','".null."','".null."','".null."','".null."','".null."')";
      /*$result=@mysql_query($sql, $this->_dbdest);
      print_r($sql);*/
      break;

    case 'productos_impuestos':
      $sql = "insert into productos_impuestos (productos_id,impuestos_id, porcentaje)
        values ('".$idDir."','".$data->impuestos_id."','".$data->porcentaje."')";
      break;

    case 'productos_impuestos_locales':
      $sql = "insert into productos_impuestos_locales (producto_id,impuesto_id)
        values ('".$idDir."','".$idEmpresa."')";
      //$result=@mysql_query($sql, $this->_dbdest);
      //print_r(mysql_error());
      break;

    case 'impuestos_locales_empresa':
      $sql = "insert into impuestos_locales_empresa (empresas_id,descripcion,tasa,importe,efecto)
        values ('".$idEmpresa."','".$data->descripcion."','".$data->tasa."','".$data->importe."','".$data->efecto."')";
      /*$result=@mysql_query($sql, $this->_dbdest);
      print_r(mysql_error());*/
      break;

    case 'pedimentos_aduanales':
      $sql = "insert into pedimentos_aduanales (pedimento,numero,fecha,empresas_id,sucursales_id)
        values ('".$data->pedimento."','".$data->numero."','".$data->fecha."','".$idEmpresa."','".$data->sucursales_id."')";
      break;

    case 'producto_pedimentos':
      $sql = "insert into producto_pedimentos (productos_id,pedimentos_aduanales_id,pedimento,numero,fecha)
        values ('".$idDir."','".$idEmpresa."','".$data->pedimento."','".$data->numero."','".$data->fecha."')";
      break;

    case 'usuarios':
      $bloqueo = "null";
      if (!empty($data->bloque_cuenta))
        $bloqueo = "'".$data->bloque_cuenta."'";

      $sql = "insert into usuarios (nombre_usuario,password,token_temp,nombre,apellido_p,apellido_m,telefono_1,email,empresas_id,sucursales_id,roles_id,usuario_status_id,ultimo_acceso,bloqueo_cuenta,ultimo_cambio_password,confirm_registration,recuperar_wizard,origenContacto,permiso_cancelacion,codigo_activacion,recover_password)
        values ('".$data->nombre_usuario."','".$data->password."','".$data->token_temp."','".$data->nombre."','".$data->apellido_p."','".$data->apellido_m."','".$data->telefono_1."','".$data->email."','".$idEmpresa."',".$idDir.",'".$data->roles_id."','".$data->usuario_status_id."','".$data->ultimo_acceso."',".$bloqueo.",'".$data->ultimo_cambio_password."','".$data->confirm_registration."','".$data->recuperar_wizard."','".$data->origenContacto."','".$data->permiso_cancelacion."','".$data->codigo_activacion."','".$data->recover_password."')";
      break;
    case deafult:
      break;
    }
    //mysql_query("SET CHARACTER SET utf8");
    mysql_query("SET NAMES 'utf8'");
    $result=@mysql_query($sql, $this->_dbdest);
    if (!$result) {
      array_push($this->msgError, $sql."\n".mysql_error());
      return false;
    }

    $id = mysql_insert_id();
    return $id;
  }

  public function saveLog($text)
  {
    $fecha = new DateTime();
    $fe =  $fecha->getTimestamp();
    $text = "[ ".date("Y-m-d H:i:s", $fe).substr((string)microtime(), 1, 4)."\n".$text;
    $folderLog = "docs/logs/".date('Y-m-d', $fe).".log";
    file_put_contents($folderLog, $text."\n", FILE_APPEND);
  }

  private function searchByKeyPattern($key, $pattern)
  {
    $len = $this->redis->hlen($key);
    return $this->redis->hscan($key, 0, array('MATCH' => $pattern, 'COUNT' => $len));
  }
}