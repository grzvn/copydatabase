<?php

require_once "lib/Connection.php";

if (empty($argv[1]))
  die("Falta el rfc de la empresa\n");

$rfc = $argv[1];
$env = (object)parse_ini_file("config/env.ini");
$origenCSD = $env->gtiOrigen .'/private/'.trim($rfc).'/csd/';
$dirNameEmpresa = (dirname(getcwd())).'/private/'.trim($rfc);
$destinoCSD = $dirNameEmpresa .'/csd/';

if (!file_exists($env->gtiOrigen))
  die("No existe la carpeta origen: ".$env->gtiOrigen."\n");

$DB = new Connection();
if (!$DB->connect_origen()) {
  print_r($DB->getErrorMsg()."\n");
  $DB->saveLog(print_r(print_r($DB->getErrorMsg(), true), true));
  exit;
}

$DB_DEST = new Connection();
if (!$DB_DEST->connect_dest()) {
  print_r($DB_DEST->getErrorMsg()."\n");
  $DB_DEST->saveLog(print_r(print_r($DB_DEST->getErrorMsg(), true), true));
  exit;
}

$empresaExiste = $DB_DEST->existeEmpresa($rfc);
if(!empty($empresaExiste)){
  printf("La empresa ". trim($rfc) . " ya existe en la BD \n");
}
else{
  if(!file_exists($dirNameEmpresa)){
    if(mkdir($dirNameEmpresa, 0300)){
      mkdir($dirNameEmpresa.'/comprobantes', 0700);
      mkdir($dirNameEmpresa.'/comprobantes/cfd', 0700);
      mkdir($dirNameEmpresa.'/comprobantes/cfd/xml', 0700);
      mkdir($dirNameEmpresa.'/comprobantes/cfd/xml/temp', 0700);
      mkdir($dirNameEmpresa.'/comprobantes/cfdi', 0700);
      mkdir($dirNameEmpresa.'/comprobantes/cfdi/xml', 0700);
      mkdir($dirNameEmpresa.'/comprobantes/cfdi/xml/temp', 0700);
      rec_copy($origenCSD, $destinoCSD);
      //mkdir($dirNameEmpresa.'/csd', 0700);
      mkdir($dirNameEmpresa.'/images',0700);
      mkdir($dirNameEmpresa.'/images/productos',0700);
      exec("chown -R www-data:www-data " . $dirNameEmpresa);
      printf("Las carpetas fueron creadas con éxito \n");
    }
  }
  else{
    die("Las carpetas ya existen \n");
  }

  $query = "select * from empresas as e left join empresa_regimenes_fiscales erf on (e.id = erf.empresas_id) left join sellos_digitales sd on (e.id = sd.empresas_id) where rfc = '" . trim($rfc) . "' and sd.sellos_status_id = 1";
  $empresa = $DB->executeSelect($query);

  if ($empresa === false) {
    print_r($DB->getErrorMsg());
    printf('No existe datos para la empresa '. trim($rfc). "\n");
    $DB->saveLog(print_r(print_r($DB->getErrorMsg(), true), true));
    exit;
  }

  $query_usuariosE = "select * from usuarios where sucursales_id is NULL and empresas_id = '" . trim($empresa[0]->empresas_id) . "'";
  $usuariosE = $DB->executeSelect($query_usuariosE);

  $query_clientes = "select * from clientes as c left join cliente_impuestos ci on (c.id = ci.clientes_id) where empresas_id = '" . trim($empresa[0]->empresas_id) . "'";
  $clientes = $DB->executeSelect($query_clientes);

  $query_productos = "select * from productos where empresas_id = '" . trim($empresa[0]->empresas_id) . "'";
  $productos = $DB->executeSelect($query_productos);

  ///////////////////////////////////////////////////

  $query_complementos = "select * from empresas_complementos where empresas_id = '" . trim($empresa[0]->empresas_id) . "'";
  $complementos = $DB->executeSelect($query_complementos);
  
  $query_direccion = "select d.*, p.codigo as pais from direcciones d left join paises p on p.id = d.paises_id where d.id = '" . trim($empresa[0]->direcciones_id) . "'";
  $direccion = $DB->executeSelect($query_direccion);

  $DB_DEST->beginTransaction();

  $direccionId = $DB_DEST->insert('direcciones', $direccion[0], $direccion, ' ');
  $empresaId = $DB_DEST->insert('empresas', $empresa[0], $direccionId, '');
  $empresaRF = $DB_DEST->insert('empresa_regimenes_fiscales', $empresa[0], $direccionId, $empresaId);


  $query_impuestos_loc = "select * from impuestos_locales_empresa where empresas_id = '" . trim($empresa[0]->empresas_id) . "'";
  $impuestos_loc = $DB->executeSelect($query_impuestos_loc);

  foreach ($impuestos_loc as $impuesto) {
    $ImpuestoId = $DB_DEST->insert('impuestos_locales_empresa', $impuesto, ' ', $empresaId);
  }

  foreach ($productos as $producto){
    $query_productos_impuestos = "select * from productos_impuestos where productos_id = '" . $producto->id . "'";
    $productos_imp = $DB->executeSelect($query_productos_impuestos);

    $query_productos_pedimentos = "select * from producto_pedimentos as pp left join pedimentos_aduanales pa on (pp.pedimentos_aduanales_id = pa.id) where productos_id = '" . $producto->id . "'";
    $productos_ped = $DB->executeSelect($query_productos_pedimentos);
    

    $productoId = $DB_DEST->insert('productos', $producto, '', $empresaId);

    if(!empty($productos_imp))
      foreach ($productos_imp as $p_imp)
        $pImpId = $DB_DEST->insert('productos_impuestos', $p_imp, $productoId, $empresaId);


    $query_productos_impuestos_loc = "select * from productos_impuestos_locales where producto_id = '" . $producto->id . "'";
    $productos_imp_loc = $DB->executeSelect($query_productos_impuestos_loc);
    //print_r($query_productos_impuestos_loc);
    //die();
    
    if(!empty($productos_imp_loc))
      foreach ($productos_imp_loc as $p_imp_loc)
        $pImpId = $DB_DEST->insert('productos_impuestos_locales', $p_imp_loc, $productoId, $ImpuestoId);

    if(!empty($productos_ped)){
      foreach ($productos_ped as $p_ped){
        $pedimentoId = $DB_DEST->insert('pedimentos_aduanales', $p_ped, ' ', $empresaId);
        $pPedId = $DB_DEST->insert('producto_pedimentos', $p_ped, $productoId, $pedimentoId);
      }
    }
  }

  foreach ($empresa as $emp){
    $query_folios = "select * from folios where sellos_digitales_id = '" . $emp->id . "' and empresas_id = '" . trim($empresa[0]->empresas_id) . "'";
    $folios = $DB->executeSelect($query_folios);

    $selloId = $DB_DEST->insert('sellos_digitales', $emp, $direccionId, $empresaId);

    if(!empty($folios))
      foreach ($folios as $folio)
        $folioId = $DB_DEST->insert('folios', $folio, $selloId, $empresaId);
  }

  foreach ($usuariosE as $usuario)
    $usuarioID = $DB_DEST->insert('usuarios', $usuario, 'NULL', $empresaId);

  foreach ($complementos as $c) {
    $empresaCom = $DB_DEST->insert('empresas_complementos', $c, $direccionId, $empresaId);
  }

  $query_sucursales = "select * from sucursales where empresas_id = '" . trim($empresa[0]->empresas_id) . "'";
  $sucursales = $DB->executeSelect($query_sucursales);

  foreach ($sucursales as $sucursal) {
    $query_direccion_sucursales = "select d.*, p.codigo as pais from direcciones d left join paises p on p.id = d.paises_id where d.id = '" . trim($sucursal->direcciones_id) . "'";
    $direcciones_sucursales = $DB->executeSelect($query_direccion_sucursales);

    $query_usuarios = "select * from usuarios where sucursales_id = '" . trim($sucursal->id) . "' and empresas_id = '" . trim($empresa[0]->empresas_id) . "'";
    $usuarios = $DB->executeSelect($query_usuarios);

    $direccionesId = $DB_DEST->insert('direcciones', $direcciones_sucursales[0], $direccion, $empresaId);

    $sucursalId = $DB_DEST->insert('sucursales', $sucursal, $direccionesId, $empresaId);

    foreach ($usuarios as $usuario) {
      $usuarioID = $DB_DEST->insert('usuarios', $usuario, $sucursalId, $empresaId);
    }
  }

  foreach ($clientes as $cliente) {
    $query_direccion_clientes = "select d.*, p.codigo as pais from direcciones d left join paises p on p.id = d.paises_id where d.id = '" . trim($cliente->direcciones_id) . "'";
    $direcciones_clientes = $DB->executeSelect($query_direccion_clientes);
    $direccionesCliId = $DB_DEST->insert('direcciones', $direcciones_clientes[0], $direccion, $empresaId);
    $clienteId = $DB_DEST->insert('clientes', $cliente, $direccionesCliId, $empresaId);

    if(!empty($cliente->impuestos_id))
      $clienteImp = $DB_DEST->insert('cliente_impuestos', $cliente, $clienteId, $empresaId);
    //print_r($direcciones_clientes);
    //die();
    $query_alias = "select * from clientes_alias where clientes_id = '" . trim($cliente->id) . "'";
    $alias = $DB->executeSelect($query_alias);
    if(!empty($alias)){
      foreach ($alias as $ali) {
        $query_direccion_alias = "select d.*, p.codigo as pais from direcciones d left join paises p on p.id = d.paises_id where d.id = '" . trim($ali->direcciones_id) . "'";
        $direcciones_alias = $DB->executeSelect($query_direccion_alias);
        foreach ($direcciones_alias as $al) {
          $direccionesAliasId = $DB_DEST->insert('direcciones', $al, $direccion, $empresaId);
        }
        $aliasId = $DB_DEST->insert('clientes_alias', $ali, $direccionesAliasId, $clienteId);
      }
    }
  }

  $error = $DB_DEST->getErrorMsg();

  if (!empty($error)) {
    echo $query."\n";
    $DB_DEST->rollbackTransaction();
    $DB_DEST->close();
    print_r($DB_DEST->getErrorMsg());
    printf("Hubo algún rror \n");
    $DB_DEST->saveLog(print_r(print_r($DB_DEST->getErrorMsg(), true), true));
    custom_rmdir($dirNameEmpresa);
    exit;
  }
  else{
    $DB_DEST->commitTransaction();
    printf("Datos ingresados con éxito \n");
    $DB_DEST->close();
  }
}

function custom_rmdir($dir) {
  if (is_file($dir)) {
    return @unlink($dir);
  }
  elseif (is_dir($dir)) {
    $scan = array_diff(glob(rtrim($dir, '/').'/{,.}*', GLOB_BRACE), glob(rtrim($dir, '/').'/{..,.}', GLOB_BRACE));
    foreach($scan as $path) {
      custom_rmdir($path);
    }
    return @rmdir($dir);
  }
}


function rec_copy($src,$dst) {
  $dir = opendir($src);
  @mkdir($dst);
  while(false !== ( $file = readdir($dir)) ) {
    if (( $file != '.' ) && ( $file != '..' )) {
      if ( is_dir($src . '/' . $file) ) {
        recurse_copy($src . '/' . $file,$dst . '/' . $file);
      }
      else {
        copy($src . $file, $dst . $file);
      }
    }
  }
  closedir($dir);
}